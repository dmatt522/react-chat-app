import React, { PropTypes } from 'react';
import classnames from 'classnames';
import { Button } from 'react-bootstrap';

const ChannelListItem = (props) => {  
  const { channel: selectedChannel, onClick, channel } = props;
  return (
    <Button bsSize="xsmall" bsClass="btn btn-xs btn-primary btn-channel">
      <a className={classnames({ selected: channel === selectedChannel })}
         style={{ cursor: 'hand', color: 'white'}}
         onClick={() => onClick(channel)}>
        <li style={{textAlign: 'left', cursor: 'pointer', marginLeft: '2em'}}>
          <h5 style={{textTransform: 'lowercase'}}>#&nbsp;{channel.name}</h5>
        </li>
      </a>
    </Button>
  );
}

ChannelListItem.propTypes = {
  channel: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
}

export default ChannelListItem;
