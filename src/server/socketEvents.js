exports = module.exports = function(io) {
  io.on('connection', function(socket) {
    socket.join('general');
    socket.on('chatMounted', function(user) {
      // TODO: Does the server need to know the user?
      socket.emit('receiveSocket', socket.id)
    })
    socket.on('leaveChannel', function(channel) {
      socket.leave(channel)
    })
    socket.on('joinChannel', function(channel) {
      socket.join(channel.name)
    })
    socket.on('newMessage', function(msg) {
      socket.broadcast.to(msg.channelID).emit('newBcMessage', msg);
    });
    socket.on('newChannel', function(channel) {
      socket.broadcast.emit('newChannel', channel)
    });
    socket.on('typing', function (data) {
      socket.broadcast.to(data.channel).emit('typingBc', data.user);
    });
    socket.on('stopTyping', function (data) {
      socket.broadcast.to(data.channel).emit('stopTypingBc', data.user);
    });
    socket.on('newPrivateChannel', function(socketID, channel) {
      socket.broadcast.to(socketID).emit('receivePrivateChannel', channel);
    })
  });
}
